monolog_baseurl_processor

To enable, you need to add 'base_url' to your `monolog.processors` parameter. 
This can be done with `services.yml`:

`monolog.processors: ['message_placeholder', 'current_user', 'request_uri', 'ip', 'referer', 'filter_backtrace', 'base_url']`

You can then use the variable in your Monolog formatter, such as:

```yaml
services:
  my.formatter:
    class: Monolog\Formatter\LineFormatter
    arguments:
      - 'BASE URL: %%extra.base_url, MESSAGE: %%message%%'
```
