<?php

namespace Drupal\monolog_baseurl_processor\Logger\Processor;

final class MonologBaseUrlProcessor {

    public function __invoke(array $record) {
        global $base_url;

        $record['extra']['base_url'] = $base_url;

        return $record;
    }

}
